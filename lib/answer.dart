import 'package:flutter/material.dart';


// controls the answer buttons on main screen
// takes handler function as a parameter which
// is used on button press
class AnswerBtn extends StatelessWidget {
  final Function selectionHandler;
  final String answerText;

  AnswerBtn(this.selectionHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: Text(answerText),
        onPressed: selectionHandler,
      ),
    );
  }
}
