import 'package:flutter/material.dart';
import './question.dart';
import './answer.dart';

// handles rendering the questions and answers list
class QuizColumn extends StatelessWidget {
  final List<Map<String, Object>> questionList;
  final int questionIndex;
  final Function answerQuestion;

  QuizColumn(
      {@required this.questionList,
      @required this.questionIndex,
      @required this.answerQuestion});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(questionList[questionIndex]['questionText']),
        ...(questionList[questionIndex]['answers'] as List<Map<String,Object>>)
            .map((answer) {
          return AnswerBtn(() => answerQuestion(answer['score']), answer['text']);
        }).toList()
      ],
    );
  }
}
