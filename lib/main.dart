import 'package:flutter/material.dart';
import './quizColumn.dart';
import './result.dart';

void main() => runApp(MyApp());

// Main app widget, uses the material stateful widget
// for more info on stateful widget see:
// https://api.flutter.dev/flutter/widgets/StatefulWidget-class.html
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  final _questionList = const [
    {
      'questionText': 'What\'s your favourite colour?',
      'answers': [
        {'text': 'Red', 'score': 10},
        {'text': 'Green', 'score': 5},
        {'text': 'Blue', 'score': 15},
        {'text': 'White', 'score': 0}
      ],
    },
    {
      'questionText': 'What\'s your favourite animal?',
      'answers': [
        {'text': 'Rabbit', 'score': 10},
        {'text': 'Dog', 'score': 15},
        {'text': 'Turtle', 'score': 17},
        {'text': 'Whale', 'score': 90}
      ],
    },
    {
      'questionText': 'What\'s your favourite season?',
      'answers': [
        {'text': 'Winter', 'score': 10},
        {'text': 'Spring', 'score': 15},
        {'text': 'Summer', 'score': 17},
        {'text': 'Autumn', 'score': 90}
      ],
    },
  ];

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex += 1;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('MyApp'),
        ),
        body: _questionIndex < _questionList.length
            ? QuizColumn(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questionList: _questionList,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
